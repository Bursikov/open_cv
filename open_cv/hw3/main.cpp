#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
using namespace std;
using namespace cv;

void drawHist(Mat& src, String lable) {
	int histSize = 256;
	float range[] = { 0, 256 };
	const float* histRange = { range };
	bool uniform = true, accumulate = false;
	Mat hist;
	calcHist(&src, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
	int w = 512;
	int h = 400;
	int bin_w = cvRound((double)w / histSize);
	Mat histImage(h, w, CV_8UC1, Scalar(0));
	normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	for (int i = 1; i < histSize; i++)
	{
		line(histImage, Point(bin_w*(i - 1), h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), h - cvRound(hist.at<float>(i))),
			Scalar(255), 2, 8, 0);
	}
	imshow(lable + " img", src);
	imshow(lable + " hist", histImage);
}

int main(int argc, char** argv)
{
	CommandLineParser parser(argc, argv, "{@input | lena.jpg | input image}");
	Mat src = imread(samples::findFile(parser.get<String>("@input")), IMREAD_GRAYSCALE);
	if (src.empty())
	{
		return EXIT_FAILURE;
	}

	drawHist(src, "src");
	int dim(256);
	Mat lut(1, &dim, CV_8U);
	for (int i = 0; i < 256; i++) {
		lut.at<uchar>(i) = cvRound(i * i / 255.0);
	}

	int w = 512;
	int h = 400;
	int bin_w = cvRound((double)w / dim);
	Mat lutImage(h, w, CV_8UC1, Scalar(0));
	for (int i = 1; i < 256; i++)
	{
		line(lutImage, Point(bin_w*(i - 1), cvRound((255.0 - lut.at<uchar>(i - 1)) / 255.0 * h)),
			Point(bin_w*(i), cvRound((255.0 - lut.at<uchar>(i)) / 255.0 * h)),
			Scalar(255), 2, 8, 0);
	}
	imshow("lut_img", lutImage);
	

	LUT(src, lut, src);

	drawHist(src, "prc");
	waitKey();

	return EXIT_SUCCESS;
}