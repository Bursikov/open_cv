#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
using namespace std;
using namespace cv;

void GlobalBin(Mat& src, Mat& dst) {
	int histSize = 256;
	float range[] = { 0, 256 };
	const float* histRange = { range };
	bool uniform = true, accumulate = false;
	Mat hist;
	calcHist(&src, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
	hist = hist / src.rows / src.cols;

	float sum = 0;
	float n = 0.5;
	int i = 0;
	for (; i < 256; ++i) {
		sum += hist.at<float>(i);
		if (sum >= n)
			break;
	}

	for (int j = 0; j < src.rows * src.cols; j++) {
		dst.at<uchar>(j) = src.at<uchar>(j) > i ? 0 : 255;
	}
}

void LocalBin(Mat& src, Mat& dst) {
	int histSize = 256;
	float range[] = { 0, 256 };
	const float* histRange = { range };
	bool uniform = true, accumulate = false;
	Mat hist;
	calcHist(&src, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
	hist = hist / src.rows / src.cols;

	float sum = 0;
	float n = 0.5;
	int i = 0;
	for (; i < 256; ++i) {
		sum += hist.at<float>(i);
		if (sum >= n)
			break;
	}

	for (int j = 0; j < src.rows * src.cols; j++) {
		dst.at<uchar>(j) = src.at<uchar>(j) > i ? 0 : 255;
	}
}

int main(int argc, char** argv)
{
	CommandLineParser parser(argc, argv, "{@input | lena.jpg | input image}");
	Mat src = imread(samples::findFile(parser.get<String>("@input")), IMREAD_GRAYSCALE);
	if (src.empty())
	{
		return EXIT_FAILURE;
	}

	Mat dst = src.clone();

	GlobalBin(src, dst);

	Mat th;
	adaptiveThreshold(src, th, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 45, 0);
	int morph_size = 1;
	Mat element = getStructuringElement(0, Size(2 * 1 + 1, 2 * morph_size + 1), Point(morph_size, morph_size));
	morphologyEx(th, th, MORPH_CLOSE, element);

	imshow("src_img", src);
	imshow("glob_bin_img", dst);
	imshow("loc_bin_img", th);

	while (true)
	{
		int c;
		c = waitKey(20);
		if ((char)c == 27)
		{
			break;
		}
	}

	return EXIT_SUCCESS;
}