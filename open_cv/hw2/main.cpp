#include <iostream>
#include <cstdlib>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

int main(int argv, char **argc)
{
	if (argv != 2) {
		std::cout << "enter image path" << std::endl;
		return -1;
	}

	std::string img_path = argc[1];
	cv::Mat image = cv::imread(img_path, cv::IMREAD_GRAYSCALE);

	if (!image.data)
	{
		std::cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	imshow("image.png", image);

	std::vector<int> compression_params;
	compression_params.push_back(cv::IMWRITE_JPEG_QUALITY);
	compression_params.push_back(95);
	bool result = false;
	try
	{
		result = imwrite("image95.jpg", image, compression_params);
	}
	catch (const cv::Exception& ex)
	{
		fprintf(stderr, "Exception converting image to JPG format: %s\n", ex.what());
	}

	compression_params.clear();
	compression_params.push_back(cv::IMWRITE_JPEG_QUALITY);
	compression_params.push_back(60);
	result = false;
	try
	{
		result = imwrite("image60.jpg", image, compression_params);
	}
	catch (const cv::Exception& ex)
	{
		fprintf(stderr, "Exception converting image to JPG format: %s\n", ex.what());
	}

	cv::Mat jpg_image;
	cv::Mat dif90;
	cv::Mat dif60;

	jpg_image = cv::imread("image95.jpg", cv::IMREAD_GRAYSCALE);
	if (!jpg_image.data)
	{
		std::cout << "Could not open or find the image" << std::endl;
		return -1;
	}
	imshow("image95.jpg", jpg_image);

	dif90 = image - jpg_image;
	imshow("image95_dif.jpg", dif90);

	jpg_image = cv::imread("image60.jpg", cv::IMREAD_GRAYSCALE);
	if (!jpg_image.data)
	{
		std::cout << "Could not open or find the image" << std::endl;
		return -1;
	}
	imshow("image60.jpg", jpg_image);

	dif60 = image - jpg_image;

	double min, max;
	cv::minMaxLoc(cv::min(dif60, dif90), &min, &max);

	dif60 = dif60 / max * 255;
	dif90 = dif90 / max * 255;
	imshow("image60_dif.jpg", dif60);
	imshow("image95_dif.jpg", dif90);
	

	cv::waitKey(0);
	return 0;
}